@extends('layouts.app')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    @isset($metaTitle)
                        {{ $metaTitle}}
                    @endisset
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="{{ route('home') }}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('users.index') }}">Users</a>
                    </li>
                    <li class="breadcrumb-item active">{{ $metaTitle }}</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-users mr-1"></i>
                            @isset($metaTitle)
                                {{ $metaTitle }}
                            @endisset
                        </h3>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        @include('partials.status')
                        <form action="{{ route('users.update', $user->id) }}" method="post">
                            @csrf
                            @method('put')
                            <div class="form-group row">
                                <div class="col-12">
                                    <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror" placeholder="First name" value="{!! old('first_name', optional($user)->first_name) !!}">
                                </div>

                                @error('first_name')
                                    <div class="col-12">
                                        <small class="form-text text-danger">
                                        {{ $message }}
                                        </small>
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" placeholder="Last name" value="{!! old('last_name', optional($user)->last_name) !!}">
                                </div>

                                @error('last_name')
                                    <div class="col-12">
                                        <small class="form-text text-danger">
                                        {{ $message }}
                                        </small>
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{!! old('email', optional($user)->email) !!}">
                                </div>
                                @error('email')
                                    <div class="col-12">
                                        <small class="form-text text-danger">
                                        {{ $message }}
                                        </small>
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <select class="form-control" name="role_id">
                                        <option>
                                            Select user's role
                                        </option>
                                        @foreach ($roles as $role)
                                            @if (is_null($user->role))
                                                <option value="{{ $role->id }}" {{ old('role_id') === $role->id ? 'selected' : '' }}>
                                                    {{ $role->name }}
                                                </option>
                                            @else
                                                <option value="{{ $role->id }}" {{ $user->role->id === $role->id ? 'selected' : '' }}>
                                                    {{ $role->name }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                @error('role_id')
                                    <div class="col-12">
                                        <small class="form-text text-danger">
                                        {{ $message }}
                                        </small>
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                                </div>
                                @error('password')
                                    <div class="col-12">
                                        <small class="form-text text-danger">
                                        {{ $message }}
                                        </small>
                                    </div>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                        </form>
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
            </section>
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
@endsection

