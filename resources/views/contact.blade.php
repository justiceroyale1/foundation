@extends('layouts.main')

@section('content')
    @include('partials.other-hero-section')

    @include('partials.contact-section')

    @include('partials.footer-section')
@endsection
