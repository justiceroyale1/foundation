@extends('layouts.main')

@section('content')
    @include('partials.other-hero-section')

    @include('partials.full-gallery-section')

    @include('partials.volunteer-section')
    @include('partials.footer-section')
@endsection
