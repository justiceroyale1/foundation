@extends('layouts.main')

@section('content')
    @include('partials.other-hero-section')

    @include('partials.request-help-section')

    @include('partials.footer-section')
@endsection
