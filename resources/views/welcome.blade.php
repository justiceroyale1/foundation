@extends('layouts.main')

@section('content')
    @include('partials.hero')

    @include('partials.counter-section')

    @include('partials.intro-section')

    @include('partials.causes-section')
    @include('partials.latest-donations-section')
    {{-- @include('partials.recent-blog-section') --}}
    @include('partials.latest-events-section')
    @include('partials.volunteer-section')
    @include('partials.footer-section')
@endsection
