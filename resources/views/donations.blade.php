@extends('layouts.main')

@section('content')
    @include('partials.other-hero-section')

    @include('partials.full-donations-section')

    @include('partials.volunteer-section')
    @include('partials.footer-section')
@endsection
