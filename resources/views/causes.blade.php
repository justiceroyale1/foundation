@extends('layouts.main')

@section('content')
    @include('partials.other-hero-section')

    @include('partials.full-causes-section')

    @include('partials.volunteer-section')
    @include('partials.footer-section')
@endsection
