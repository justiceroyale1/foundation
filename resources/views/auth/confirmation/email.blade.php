@extends('layouts.front')

@section('styles')
    <link href="{{ asset('vendor/bootstrap-form-helpers/dist/css/bootstrap-formhelpers.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

  <div class="banner w-100">
    @include('layouts.partials.front.nav')
    @include('layouts.partials.front.confirmation.email')
  </div>
  @include('layouts.partials.front.footer')
@endsection

@push('scripts')
    <script src="{{ asset('vendor/bootstrap-form-helpers/dist/js/bootstrap-formhelpers.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-form-helpers/js/bootstrap-formhelpers-countries.js') }}"></script>
@endpush
