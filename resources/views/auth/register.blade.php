@extends('layouts.front')
@push('css')
    <link href="{{ asset('vendor/bootstrap-form-helpers/dist/css/bootstrap-formhelpers.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('title')
	<title>{{ config('app.name') }} - Register</title>
	<meta name="description" content="Sportswin247 registration page.">
@endsection

@section('content')
	@component('layouts.components.title')
		@slot('title')
			{{-- You always win with our tips --}}
		@endslot
	@endcomponent

	<div class="row">
	    <div class="col-lg-10 col-lg-offset-1 col-xs-12">
	    		@if (session('status'))
	    		     @include('layouts.partials.alert.success')
	    		@endif
	    		@if (session('email'))
	    		     @include('layouts.partials.alert.email')
	    		@endif
	            <form action="{{ route('register') }}" method="post">
	            	{{ csrf_field() }}
	            	<div class="box box-primary">
	            	    <div class="box-header">
	            	        <h3 class="text-center">Register</h3>
	            	        <h5 class="text-center">All fields are required</h5>
	            	    </div>
	            	    <div class="box-body">
	            	    	<div class="row margin">
	            	    		<div class="form-group col-md-6 col-xs-12 {{ $errors->has('first-name') ? 'is-invalid' : '' }}">
	            	    			<input type="text" name="first-name" class="form-control" placeholder="First name" value="{{ old('first-name') }}" autofocus required />
	            	    			@if ($errors->has('first-name'))
	            	    			  <div class="help-block">
	            	    			    <strong>{{ $errors->first('first-name') }}</strong>
	            	    			  </div>
	            	    			@endif
	            	    		</div>
	            	    		<div class="form-group col-md-6 col-xs-12 {{ $errors->has('last-name') ? 'is-invalid' : '' }}">
	            	    			<input type="text" name="last-name" class="form-control" placeholder="Last name" value="{{ old('last-name') }}" required />
	            	    			@if ($errors->has('last-name'))
	            	    			  <div class="help-block">
	            	    			    <strong>{{ $errors->first('last-name') }}</strong>
	            	    			  </div>
	            	    			@endif
	            	    		</div>
	            	    	</div>
	            	    	<div class="row margin">
	            	    		<div class="form-group col-md-6 col-xs-12 {{ $errors->has('email') ? 'has-error' : '' }}">
	            	    			<input type="text" name="email" class="form-control" placeholder="Email address" value="{{ old('email') }}" required />
	            	    			@if ($errors->has('email'))
	            	    			  <div class="help-block">
	            	    			    <strong>{{ $errors->first('email') }}</strong>
	            	    			  </div>
	            	    			@endif
	            	    		</div>
	            	    		<div class="form-group col-md-6 col-xs-12 {{ $errors->has('password') ? 'has-error' : '' }}">
	            	    			<input type="password" name="password" class="form-control" placeholder="Password" required />
	            	    			@if ($errors->has('password'))
	            	    			  <div class="help-block">
	            	    			    <strong>{{ $errors->first('password') }}</strong>
	            	    			  </div>
	            	    			@endif
	            	    		</div>
	            	    	</div>
	            	    	<div class="row margin">
	            	    		<div class="form-group col-md-6 col-xs-12 {{ $errors->has('password') ? 'has-error' : '' }}">
	            	    			<input type="password" name="password_confirmation" class="form-control" placeholder="Retype password" required />
	            	    		</div>
	            	    		<div class="col-md-6 col-xs-12">
	            	    			<select name="country" class="input-medium bfh-countries form-control" data-country="{{  old('country') ? old('country') : 'US' }}" required></select>
	            	    		</div>
	            	    	</div>
	            	    	<div class="row margin">
	            	    		<small class="muted">
	            	    			By clicking on the 'Register' button below, you consent to have read and accepted our <a href="{{ route('terms') }}">terms and conditions</a> as well as our <a href="{{ route('privacy') }}">Privacy Policy</a>.
	            	    		</small>
	            	    	</div>
	            	    	<div class="row margin">
	            	    		<div class="col-md-8 col-md-offset-2 col-xs-12">
	            	    			<button type="submit" class="btn btn-success btn-block">Register</button>
	            	    		</div>
	            	    	</div>
	            	    </div><!-- /.box-body-->
	            	    <div class="box-footer">
	            	    	<a href="{{ route('login') }}" class="text-center">I&apos;ve already registered</a>
	            	    </div>
	            	</div><!-- /.box -->
	            </form>

	            {{-- <div class="margin text-center">
	                <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
	                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
	                <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

	            </div> --}}
	    </div><!-- /.col -->
	</div><!-- /.row -->
@endsection
@push('js')
    <script src="{{ asset('vendor/bootstrap-form-helpers/dist/js/bootstrap-formhelpers.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-form-helpers/js/bootstrap-formhelpers-countries.js') }}"></script>
@endpush

