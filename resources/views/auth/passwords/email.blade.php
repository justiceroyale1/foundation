@extends('layouts.front')
@push('css')
    <link href="{{ asset('vendor/bootstrap-form-helpers/dist/css/bootstrap-formhelpers.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('title')
	<title>{{ config('app.name') }} - Password Reset Request</title>
	<meta name="description" content="Sportswin247 password reset request page.">
@endsection

@section('content')
	@component('layouts.components.title')
		@slot('title')
			{{-- This is needed to add some spacing to the top --}}
		@endslot
	@endcomponent

	<div class="row">
	    <div class="col-lg-10 col-lg-offset-1 col-xs-12">
	    		@if (session('status'))
	    		     @include('layouts.partials.alert.success')
	    		@endif
	    		@if (session('email'))
	    		     @include('layouts.partials.alert.email')
	    		@endif
	            <form action="{{ route('password.email') }}" method="post">
	            	{{ csrf_field() }}
	            	<div class="box box-primary">
	            	    <div class="box-header">
	            	        <h3 class="text-center">Request Password Reset</h3>
	            	        <h5 class="text-center">We'd send you a password reset link</h5>
	            	    </div>
	            	    <div class="box-body">
	            	    	<div class="row margin">
	            	    		<div class="form-group col-md-6 col-md-offset-3 col-xs-12 {{ $errors->has('email') ? 'has-error' : '' }}">
	            	    			<input type="text" name="email" class="form-control" placeholder="Email address" value="{{ old('email') }}" required />
	            	    			@if ($errors->has('email'))
	            	    			  <div class="help-block">
	            	    			    <strong>{{ $errors->first('email') }}</strong>
	            	    			  </div>
	            	    			@endif
	            	    		</div>
	            	    	</div>
	            	    	<div class="row margin">
	            	    		<div class="col-md-8 col-md-offset-2 col-xs-12">
	            	    			<button type="submit" class="btn btn-success btn-block">Send Link</button>
	            	    		</div>
	            	    	</div>
	            	    </div><!-- /.box-body-->
	            	</div><!-- /.box -->
	            </form>
	    </div><!-- /.col -->
	</div><!-- /.row -->
@endsection