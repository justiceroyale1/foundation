@extends('layouts.main')

@section('content')
    @include('partials.other-hero-section')

    @include('partials.about-section')

    @include('partials.counter-section')

    @include('partials.latest-donations-section')
    {{-- @include('partials.recent-blog-section') --}}
    @include('partials.footer-section')
@endsection
