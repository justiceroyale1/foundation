@extends('layouts.main')

@section('content')
    @include('partials.other-hero-section')

    @include('partials.full-events-section')

    @include('partials.footer-section')
@endsection
