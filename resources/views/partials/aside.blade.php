<aside class="main-sidebar sidebar-dark-success elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        {{-- <img src="{{ asset('adminlte-3/assets/images/logo.png') }}" alt="Evansville Logo" class="brand-image img-circle elevation-3"> --}}
        <span class="brand-text font-weight-bold">
            {{ config('app.name') }}
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('adminlte-3/assets/images/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div> --}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link {{ $currentUrl == route('home') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                @can('create', App\User::class)
                    <li class="nav-item">
                        <a href="{{ route('users.index') }}" class="nav-link {{ Str::contains($currentUrl, [
                            route('users.index'),
                            route('users.create')]) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Users
                            </p>
                        </a>
                    </li>
                @endcan
                {{-- <li class="nav-item">
                    <a href="{{ route('grade-levels.index') }}" class="nav-link {{ $currentUrl == route('grade-levels.index') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-layer-group"></i>
                        <p>
                            Grade Levels
                        </p>
                    </a>
                </li> --}}
                {{-- <li class="nav-item has-treeview {{ Str::contains($currentUrl, [
                    route('recurrent-budget-performances.index'),
                    route('recurrent-budget-performances.create'),
                    route('recurrent-budget-proposals.index'),
                    route('recurrent-budget-proposals.create'),
                    route('recurrent-categories.index'),
                    route('recurrent-sub-categories.index'),
                    route('recurrent-sub-categories.create'),
                    route('recurrent-headings.index')] )  ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-recycle"></i>
                    <p>
                        Recurrent Budget
                        <i class="fas fa-angle-left right"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('recurrent-categories.index') }}" class="nav-link {{ Str::contains($currentUrl, route('recurrent-categories.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('recurrent-sub-categories.index') }}" class="nav-link {{ Str::contains($currentUrl, route('recurrent-sub-categories.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sub-Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('recurrent-headings.index') }}" class="nav-link {{ Str::contains($currentUrl, route('recurrent-headings.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Headings</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('recurrent-budget-proposals.index') }}" class="nav-link {{ Str::contains($currentUrl, route('recurrent-budget-proposals.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Budget Proposal</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('recurrent-budget-performances.index') }}" class="nav-link {{ Str::contains($currentUrl, route('recurrent-budget-performances.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Budget Performance</p>
                            </a>
                        </li>
                    </ul>
                </li> --}}
                {{-- <li class="nav-item has-treeview {{ Str::contains($currentUrl, [
                    route('capital-categories.index'),
                    route('capital-sub-categories.index'),
                    route('capital-budget-performances.index'),
                    route('capital-sub-categories.create')] )  ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-landmark"></i>
                        <p>
                            Capital Budget
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('capital-categories.index') }}" class="nav-link {{ Str::contains($currentUrl, route('capital-categories.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('capital-sub-categories.index') }}" class="nav-link {{ Str::contains($currentUrl, route('capital-sub-categories.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sub-Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('capital-budget-proposals.index') }}" class="nav-link {{ Str::contains($currentUrl, route('capital-budget-proposals.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Budget Proposal</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('capital-budget-performances.index') }}" class="nav-link {{ Str::contains($currentUrl, route('capital-budget-performances.index')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Budget Performance</p>
                            </a>
                        </li>
                    </ul>
                </li> --}}
                <li class="nav-item">
                    <a href="#" class="nav-link text-danger" data-toggle="modal" data-target="#modal-default">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Logout
                            {{-- <span class="right badge badge-danger">New</span> --}}
                        </p>
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a href="{{ route('sales.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                            Sales
                        </p>
                    </a>
                </li> --}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
