<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
  <div class="container">
      <a class="navbar-brand" href="{{ route('welcome') }}">
    {{ config('app.name') }}
  </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item {{ Route::currentRouteName() == 'welcome' ? 'active' : '' }}">
          <a href="{{ route('welcome') }}" class="nav-link">Home</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName() == 'about' ? 'active' : '' }}">
          <a href="{{ route('about') }}" class="nav-link">About</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName() == 'causes' ? 'active' : '' }}">
          <a href="{{ route('causes') }}" class="nav-link">Causes</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName() == 'donations' ? 'active' : '' }}">
          <a href="{{ route('donations') }}" class="nav-link">Donations</a>
        </li>
        {{-- <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li> --}}
        <li class="nav-item {{ Route::currentRouteName() == 'gallery' ? 'active' : '' }}">
          <a href="{{ route('gallery') }}" class="nav-link">Gallery</a>
        </li>
        {{-- <li class="nav-item {{ Route::currentRouteName() == 'events' ? 'active' : '' }}">
          <a href="{{ route('events') }}" class="nav-link">Events</a>
        </li> --}}
        <li class="nav-item {{ Route::currentRouteName() == 'request-help' ? 'active' : '' }}">
          <a href="{{ route('request-help') }}" class="nav-link">Request Help</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName() == 'contact' ? 'active' : '' }}">
          <a href="{{ route('contact') }}" class="nav-link">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <!-- END nav -->