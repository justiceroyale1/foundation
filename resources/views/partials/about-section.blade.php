<section class="ftco-section">
    <div class="container">
        <div class="row d-flex">
            <div class="col-md-6 d-flex ftco-animate">
                <div class="img img-about align-self-stretch" style="background-image: url(images/bg_3.jpg); width: 100%;"></div>
            </div>
            <div class="col-md-6 pl-md-5 ftco-animate">
                <h2 class="mb-4">Welcome to {{ config('app.name') }}</h2>
                <p>
                    We are an NGO established for the purpose of alleviately unneccessary suffering. We've seen how a staggering number of people struggle to get the most basic human neccesities such as food and healthcare.
                </p>
                <p>
                    We could no longer bear to just watch as hundreds of thousands go hungry, homeless, or sick. Therefore we decided to do something about it. Our organization focuses helping the helpless and restoring some dignity to their precious lives.
                </p>
            </div>
        </div>
    </div>
</section>