<section class="ftco-section bg-light">
    <div class="container-fluid">
        <div class="row justify-content-center mb-5 pb-3">
      <div class="col-md-5 heading-section ftco-animate text-center">
        <h2 class="mb-4">Our Causes</h2>
        <p>
            We're driven by a deep-rooted desire to end unnecessary suffering and hardship.
            Below are some of our causes.
        </p>
      </div>
    </div>
        <div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="carousel-cause owl-carousel">
                    <div class="item">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url(images/cause-1.jpg);"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Clean water for the rural areas</a></h3>
                                <p>
                                    Water is life. Sadly, many do not have access to clean drinking water in most rural areas.
                                </p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url(images/cause-2.jpg);"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Homes for internally displaced persons</a></h3>
                                <p>
                                    Many people have been displaced from their home dues to conflict, natural disiasters.
                                </p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url(images/cause-3.jpg);"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Orphanage</a></h3>
                                <p>
                                    Catering for the needs of kids who have no one to do so for them.
                                </p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url(images/cause-4.jpg);"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Medical facilities</a></h3>
                                <p>
                                    We plan to set up medical facilities with modern equipment at strategic locations where they can aid those that need them.
                                </p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url(images/cause-5.jpg);"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Disabled Aid</a></h3>
                                <p>
                                    We're committed to supporting disabled persons while the right tools that to need to overcome the hurdles they're confronted with.
                                </p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                    <div class="progress custom-progress-success">
                      <div class="progress-bar bg-primary" role="progressbar" style="width: 28%" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url(images/cause-6.jpg);"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Education & Scholarships</a></h3>
                                <p>
                                    At least 2 out of every 3 child is out of school today.
                                </p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                    <div class="progress custom-progress-success">
                      <div class="progress-bar bg-primary" role="progressbar" style="width: 28%" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>