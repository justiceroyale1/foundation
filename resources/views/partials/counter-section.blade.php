<section class="ftco-counter ftco-intro" id="section-counter">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-5 d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 color-1 align-items-stretch">
          <div class="text">
              <span>Served Over</span>
            <strong class="number" data-number="1432805">0</strong>
            <span>People nationwide</span>
          </div>
        </div>
      </div>
      <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 color-2 align-items-stretch">
          <div class="text">
              <h3 class="mb-4">Make Donation</h3>
              <p>Every cent goes a long way.</p>
              <p><a href="#" class="btn btn-white px-3 py-2 mt-2">Donate Now</a></p>
          </div>
        </div>
      </div>
      <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 color-3 align-items-stretch">
          <div class="text">
              <h3 class="mb-4">Be a Volunteer</h3>
              <p>Help us help more people.</p>
              <p><a href="#" class="btn btn-white px-3 py-2 mt-2">Be A Volunteer</a></p>
          </div>
        </div>
      </div>
        </div>
    </div>
</section>