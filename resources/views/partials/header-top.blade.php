<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 text-left fh5co-link">
                <a href="#" class="text-decoration-none">FAQ</a>
                <a href="#">Forum</a>
                <a href="#">Contact</a>
            </div>
            <div class="col-md-6 col-sm-6 text-right fh5co-social">
                <a href="#" class=""><i class="icon-facebook2"></i></a>
                <a href="#" class=""><i class="icon-twitter2"></i></a>
                <a href="#" class=""><i class="icon-instagram2"></i></a>
            </div>
        </div>
    </div>
</div>