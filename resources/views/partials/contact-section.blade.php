<section class="ftco-section contact-section ftco-degree-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-6 pr-md-5">
          <h4 class="mb-4">Do you have any questions?</h4>
          <form action="#">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Your Name">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Your Email">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Subject">
            </div>
            <div class="form-group">
              <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
            </div>
          </form>
        </div>

        {{-- <div class="col-md-6" id="map"></div> --}}
        <div class="col-md-6">
          <div class="col-md-12 mb-4">
            <h2 class="h4">Contact Information</h2>
          </div>
          <div class="w-100"></div>
          <div class="col-md-12">
              <p>
                  <span>Address:</span>
                  {{ config('app.address') }}
              </p>
              <p>
                <span>Phone:</span>
                <a href="tel://{{ Str::replaceFirst('+', '', config('app.phone')) }}">
                    {{ config('app.phone') }}
                </a>
              </p>
              <p>
                <span>Email:</span>
                <a href="mailto:{{ config('app.email') }}">
                    {{ config('app.email') }}
                </a>
              </p>
              <p>
                <span>Website</span> 
                <a href="{{ config('app.url') }}">
                  {{ config('app.url') }}
                </a>
              </p>
          </div>
          {{-- <div class="img img-2 w-100 h-100" style="background-image: url(images/bg_8.jpg);"></div> --}}
        </div>

      </div>
    </div>
  </section>