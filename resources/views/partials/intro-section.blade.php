<section class="ftco-section">
    <div class="container">
        <div class="row">
      <div class="col-md-4 d-flex align-self-stretch ftco-animate">
        <div class="media block-6 d-flex services p-3 py-4 d-block">
          <div class="icon d-flex mb-3"><span class="flaticon-donation-1"></span></div>
            <div class="media-body pl-4">
                <h3 class="heading">Make Donation</h3>
                <p>
                    Your charitable donations go a long way to help those in need of basic aminites, such as food, and healthcare.
                </p>
            </div>
        </div>      
      </div>
      <div class="col-md-4 d-flex align-self-stretch ftco-animate">
        <div class="media block-6 d-flex services p-3 py-4 d-block">
            <div class="icon d-flex mb-3"><span class="flaticon-charity"></span></div>
            <div class="media-body pl-4">
                <h3 class="heading">Become A Volunteer</h3>
                <p>
                    Every volunteer is of immense value to our cause. Join other like-minded persons to help those in need.
                </p>
            </div>
        </div>      
      </div>
      <div class="col-md-4 d-flex align-self-stretch ftco-animate">
        <div class="media block-6 d-flex services p-3 py-4 d-block">
            <div class="icon d-flex mb-3"><span class="flaticon-donation"></span></div>
            <div class="media-body pl-4">
                <h3 class="heading">Sponsorship</h3>
                <p>
                    Sponsor one of our many programmes to partner with us as we alleviate the impacts of poverty, Women and child abuse, inadequate healthcare facilities, etc.
                </p>
            </div>
        </div>    
      </div>
    </div>
    </div>
</section>