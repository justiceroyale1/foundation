<div class="hero-wrap" style="background-image: url('images/bg_7.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-7 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
                <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">
                    <q>
                        If you can't feed a hundred people, then feed just one.
                    </q>
                </h1>
                <h1 class="mb-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">
                    -Mother Teresa
                </h1>

                {{-- <p data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">
                    <a href="https://vimeo.com/45830194" class="btn btn-white btn-outline-white px-4 py-3 popup-vimeo"><span class="icon-play mr-2"></span>Watch Video</a>
                </p> --}}
            </div>
        </div>
    </div>
</div>