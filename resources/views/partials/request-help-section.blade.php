<section class="ftco-section contact-section ftco-degree-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-6 pr-md-5">
          <h4 class="mb-4">Fill the form below</h4>
          <form action="#">
            <div class="form-group">
              <input type="text" class="form-control" name="name" placeholder="Name of Receipient (eg. John Doe)">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="phone" placeholder="Phone number of Recipient (eg. 08060890911)">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="title" placeholder="Title (eg. 200K For Hospital Bills)">
            </div>
            <div class="form-group">
              <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Explain in detail what you need help with and what you've done before now."></textarea>
            </div>
            <div class="form-group">
              <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
            </div>
          </form>
        </div>

        </div>

      </div>
    </div>
  </section>