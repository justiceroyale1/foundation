<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

trait PersistsData
{
    /**
     * Persist all the form data (except those whose keys match the
     * elements of the $except array) in the request to the
     * database table for this model.
     *
     * @param \Illuminate\Foundation\Http\FormRequest $request
     */
    public static function persist(FormRequest $request)
    {
        self::create($request->validated());
    }

    /**
     * Modify the record of the given model using all the form data
     * (except those whose keys match the elements of the $except
     * array) in the request.
     *
     * @param \Illuminate\Foundation\Http\FormRequest $request
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public static function modify(FormRequest $request, Model $model)
    {
        $model->update($request->validated());
    }
}
