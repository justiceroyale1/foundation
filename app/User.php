<?php

namespace App;

use App\Role;
use App\Traits\PersistsData;
use App\Http\Requests\PutUser;
use App\Http\Requests\PostUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class User extends Authenticatable
{
    use Notifiable, PersistsData;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The roles that belong to the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'role_users', 'user_id', 'role_id');
    }

    public function getFullnameAttribute()
    {
        return $this->first_name . " " . $this->last_name;;
    }

    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    public function hasRole($role)
    {
        $roles = $this->roles()->pluck('name');

        return $roles->contains($role);
    }

    public static function persist(PostUser $request)
    {
        $user = null;
        $data = $request->validated();

        DB::transaction(function () use($user, $data) {
            $user = self::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            $user->roles()->attach($data['role_id']);
        });

        return $user;
    }

    public static function modify(PutUser $request, User $user)
    {
        $data = $request->validated();

        DB::transaction(function () use($user, $data) {
            $user->update([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            $user->roles()->sync([$data['role_id']]);
        });

        return $user;
    }
}
