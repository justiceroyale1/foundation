<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostUser;
use App\Http\Requests\PutUser;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->authorizeResource(User::class, 'user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metaTitle = 'Users';
        $users = User::all();
        return view('users.index', compact('metaTitle','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $metaTitle = 'Add User';
        $roles = Role::all();
        return view('users.create', compact('metaTitle', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\PostUser  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostUser $request)
    {
        User::persist($request);

        return redirect()->back()->with([
            'status' => "User has been added."
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $metaTitle = 'Edit User';
        $roles = Role::all();
        return view('users.edit', compact('user', 'metaTitle', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\PutUser  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(PutUser $request, User $user)
    {
        $user = User::modify($request, $user);

        return redirect()->back()->with([
            'status' => "User has been updated."
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
