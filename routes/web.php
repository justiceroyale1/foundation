<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('about', 'AboutController@index')->name('about');
Route::get('causes', 'CauseController@index')->name('causes');
Route::get('donations', 'DonationController@index')->name('donations');
Route::get('gallery', 'GalleryController@index')->name('gallery');
// Route::get('events', 'EventController@index')->name('events');
Route::get('request-help', 'RequestHelpController@index')->name('request-help');
Route::get('contact', 'ContactController@index')->name('contact');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('users', 'UserController');
