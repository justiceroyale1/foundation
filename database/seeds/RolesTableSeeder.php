<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    protected $seeds = [
        [
            'name' => 'Administrator',
        ],
        [
            'name' => 'Regular user',
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->seeds as $seed) {
            Role::create($seed);
        }
    }
}
