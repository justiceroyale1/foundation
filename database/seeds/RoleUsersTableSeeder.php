<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RoleUsersTableSeeder extends Seeder
{
    protected $seeds = [
        [
            'email' => 'justiceroyale1@gmail.com',
            'role' => 'Administrator'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->seeds as $seed) {
            $role = Role::where('name', $seed['role'])->first();
            $user = User::where('email', $seed['email'])->first();

            $user->roles()->attach($role->id);
        }
    }
}
