<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    protected $seeds = [
        [
            'first_name' => 'Justice',
            'last_name' => 'Abutu',
            'email' => 'justiceroyale1@gmail.com',
            'password' => 'secret'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->seeds as $seed) {
            $seed['password'] = Hash::make($seed['password']);
            User::create($seed);
        }
    }
}
